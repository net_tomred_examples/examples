package net.tomred.example.gson;

import static net.tomred.example.gson.GsonLists.getDogs;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import net.tomred.example.gson.model.Dog;

/**
 * This is a code example to support the tomred.net article<br/>
 * <strong>Gson parse Json array to List</string>
 * 
 * @author Dermot Butterfield
 * 
 * @see <a href=
 *      "https://www.tomred.net/java/java-gson-parse-json-array-to-list.html">Gson
 *      parse Json array to List</a>
 */
public class GsonListsTest {

  Gson g = new Gson();
  /**
   * [{"name":"Snoopy"},{"name":"Ubu"},{"name":"Lassie"}]
   */
  final String json = g.toJson(getDogs());

  @Test
  void testUnmarshall_fails() {
    JsonSyntaxException thrown = assertThrows(JsonSyntaxException.class, () -> {
      g.fromJson(json, Dog.class);
    });
    assertTrue(thrown.getMessage().contains("Expected BEGIN_OBJECT but was BEGIN_ARRAY"));
  }

  @Test
  void testUnmarshall_succeeds_usingArray() {
    Dog[] fromJson = g.fromJson(json, Dog[].class);
    List<Dog> asList = Arrays.asList(fromJson);

    assertEquals(getDogs(), asList);
  }

  @Test
  void testUnmarshall_succeeds_usingTypeToken() {
    Type listType = new TypeToken<List<Dog>>() {
    }.getType();

    List<Dog> fromJson = g.fromJson(json, listType);
    assertEquals(getDogs(), fromJson);
  }
}