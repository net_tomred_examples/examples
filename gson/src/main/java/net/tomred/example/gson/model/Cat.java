package net.tomred.example.gson.model;

public class Cat extends Animal {

  public Cat(String name) {
    super(name);
  }

  @Override
  void makeSound() {
    System.out.println("Meow meow");
  }

}
