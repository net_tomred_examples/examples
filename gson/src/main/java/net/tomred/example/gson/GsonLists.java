package net.tomred.example.gson;

import java.util.ArrayList;
import java.util.List;

import net.tomred.example.gson.model.Dog;

public class GsonLists {

  public static List<Dog> getDogs() {
    List<Dog> dogs = new ArrayList<Dog>();
    dogs.add(new Dog("Snoopy"));
    dogs.add(new Dog("Ubu"));
    dogs.add(new Dog("Lassie"));
    return dogs;
  }

}