package net.tomred.example.gson.model;

public class Dog extends Animal {

  public Dog(String name) {
    super(name);
  }

  @Override
  void makeSound() {
    System.out.println("Woof woof woof");
  }

}
