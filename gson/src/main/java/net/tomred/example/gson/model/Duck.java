package net.tomred.example.gson.model;

public class Duck extends Animal {

  public Duck(String name) {
    super(name);
  }

  @Override
  void makeSound() {
    System.out.println("Quack quack");
  }

}
