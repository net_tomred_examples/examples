package net.tomred.example.gson.model;

import java.util.Objects;

public abstract class Animal {

  String name;

  public Animal(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  abstract void makeSound();

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Animal other = (Animal) obj;
    return Objects.equals(name, other.name);
  }
}
